## Git and GitLab basics

1. Edit 'edit_this_file.rb' in 'training-examples'
2. See it listed as a changed file (working area)
3. View the differences
4. Stage the file
5. Commit
6. Push the commit to the remote
7. View the git log

----------

```
# Edit `edit_this_file.rb`
git status
git diff
git add <file>
git commit -m 'My change'
git push origin master
git log
```
