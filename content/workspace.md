## Workspace 

- Choose a directory on you machine easy to access
- Create a workspace or development directory
- This is where we'll be working and adding content

----------

```
mkdir ~/development
cd ~/development

-or-

mkdir ~/workspace
cd ~/workspace  
```
