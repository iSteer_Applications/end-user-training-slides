## Getting Help

- Use the tools at your disposal when you get stuck.
  - Use 'git help <command>' command
  - Use Google (i.e. StackOverflow, Google groups)
  - Read documentation at https://git-scm.com
