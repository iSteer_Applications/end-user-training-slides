## Git Workflow

- Untracked files
    - New files that Git has not been told to track previously.
- Working area (Workspace)
    - Files that have been modified but are not committed.
- Staging area (Index)
    - Modified files that have been marked to go in the next commit.
- Upstream
    - Hosted repository on a shared server
